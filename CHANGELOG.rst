Change Log
==========

TODO
----

- Write aiosqlite `-auto` part
- Think about hybrid versioning in filenames
- Doc about up1 command
- Audit trail in aiosqlembic db

0.3.12
------

- Removed writable flag to the migrations directory

0.3.11
------

- Some updates post click 8

0.3.8
-----

- Just relaxed dependencies

0.3.7
-----

- Rewrote tests to use a clean db on every test.
- Upgraded most packages.

0.3.6
-----

- Fixed a bug were the `up` command was beginning always at MIN_VERSION instead of current version.

0.3.5
-----

- Fixed a bug were upTo with a version id was trying to migrate the entire revision tree.


0.3.4
-----

- Fixed a bug where a failed migration was returning a 0 status code even if errors in output were sent.

0.3.3
-----

**Breaking change**: `aiosqlembic` table was in the `public` schema, it uses now its own `aiosqlembic` schema.

- Added `up1` command
- Modified `status` command output
- The `create` command can now take a `--version_id` or `-v` parameter
- Removed entire `aiosqlite` support for now

0.3.2
-----

- Emergency fix on loop usage, revert on asyncio.run

0.3.1
-----

- Removed decorator settings
- Trying new @coro decorator

0.3.0
-----

- Added up1 command
- Removed lots of dependencies, asyncclick notably
- Upgraded most libraries

0.2.3
------

- Modified README to use markdown ...
- Make use of color in status command, but also in others
- Better messages overall

0.2.2
-----

- Changed theme, improved docs
- Added tests for various command / scenarios


0.2.1
-----

- Added up and down command, up will apply all up revisions, down will apply own revision one by one

0.2.0
-----

- Breaking the config file, now the CLI is configless
- Two commands, create and status
- Create is like the previous revision generates a .sql file with schema up and down, automatically or not
- Status reports the revisions applied or not to the database

0.1.0
-----

- Initial attempt with 2 commands init, revision
- Init for creating the config and the migrations directory
- Revision to generate .sql file from a template and auto schema