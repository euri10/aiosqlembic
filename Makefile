.DEFAULT_GOAL := help

PY_SRC := aiosqlembic/ tests/

.PHONY: build
build:  ## Build the package wheel and sdist.
	poetry build

.PHONY: check
check: check-black check-flake8 check-mypy

.PHONY: check-black
check-black:  ## Check if code is formatted nicely using black.
	black --check --diff $(PY_SRC)

.PHONY: check-flake8
check-flake8:  ## Check for general warnings in code using flake8.
	flake8 --exclude=.venv $(PY_SRC)

.PHONY: check-mypy
check-mypy:  ## Check for general warnings in code using flake8.
	mypy $(PY_SRC)

.PHONY: clean
clean: clean-tests  ## Delete temporary files.
	@rm -rf build 2>/dev/null
	@rm -rf dist 2>/dev/null
	@rm -rf aiosqlembic/*.egg-info 2>/dev/null
	@rm -rf .coverage* 2>/dev/null
	@rm -rf .pytest_cache 2>/dev/null
	@rm -rf pip-wheel-metadata 2>/dev/null

.PHONY: clean-tests
clean-tests:  ## Delete temporary tests files.
	@rm -rf tests/tmp/* 2>/dev/null


.PHONY: docs
docs:  ## Build the documentation locally.
	cd docs && sphinx-apidoc -o source/ ../aiosqlembic
	cd docs && $(MAKE) html -d

#.PHONY: docs-serve
#docs-serve:  ## Serve the documentation (localhost:8000).
#	poetry run mkdocs serve
#
#.PHONY: docs-deploy
#docs-deploy:  ## Deploy the documentation on GitHub pages.
#	poetry run mkdocs gh-deploy

.PHONY: help
help:  ## Print this help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST) | sort

.PHONY: lint
lint: lint-black  ## Run linting tools on the code.

.PHONY: lint-black
lint-black:  ## Lint the code using black.
	black $(PY_SRC)


.PHONY: publish
publish:  ## Publish the latest built version on PyPI.
	poetry publish

.PHONY: setup
setup:  ## Setup the development environment.
	poetry install

#.PHONY: readme
#readme:  ## Regenerate README.md.
#	poetry run ./scripts/gen-readme-data.py | \
#		poetry run jinja2 --strict -o README.md --format=json scripts/templates/README.md -

.PHONY: test
test: clean-tests  ## Run the tests using pytest.
	coverage run
	coverage report