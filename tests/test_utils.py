from pathlib import Path
from urllib.parse import urlparse

import pytest
from py._path.local import LocalPath

from aiosqlembic.main import MIN_VERSION, MAX_VERSION
from aiosqlembic.models.cli import RevisionPath
from aiosqlembic.utils import (
    get_revision_tree,
    filter_revision,
    get_connection,
    get_next_revision,
    get_previous_revision,
    get_current_revision,
)

# 0
# 1 **
# 2
# 3
from tests.conftest import TEST_DSN_POSTGRES

filter_revision_data = [
    (0, 1, 3, False),
    (3, 1, 3, True),
    (2, 1, 3, True),
    (2, 1, 2, True),
    (2, 1, 0, False),
    (0, 3, 2, False),
    (1, 1, 1, False),
]


@pytest.mark.parametrize(
    "version_id,current_vid,target_vid,expected", filter_revision_data
)
def test_filter_revision(
    version_id: int, current_vid: int, target_vid: int, expected: bool
) -> None:
    assert expected == filter_revision(version_id, current_vid, target_vid)


def test_get_revision_tree_max_included() -> None:
    currentdir = Path(__file__).parent
    migrations = Path(currentdir / "revisions_tests")
    revision1 = RevisionPath(
        version_id=11111111111111,
        message="revision1",
        file=Path(migrations / "11111111111111_revision1.sql"),
        next=22220222222222,
        previous=-1,
    )
    revision2 = RevisionPath(
        version_id=22220222222222,
        message="revision2",
        file=Path(migrations / "22220222222222_revision2.sql"),
        next=33330330033333,
        previous=11111111111111,
    )
    revision3 = RevisionPath(
        version_id=33330330033333,
        message="revision3",
        file=Path(migrations / "33330330033333_revision3.sql"),
        next=-1,
        previous=22220222222222,
    )
    assert get_revision_tree(migrations, MIN_VERSION, MAX_VERSION) == [
        revision1,
        revision2,
        revision3,
    ]
    assert (
        get_next_revision([revision1, revision2, revision3], 11111111111111)
        == revision2
    )
    assert (
        get_next_revision([revision1, revision2, revision3], 22220222222222)
        == revision3
    )
    assert get_next_revision([revision1, revision2, revision3], 33330330033333) is None
    assert (
        get_previous_revision([revision1, revision2, revision3], 11111111111111) is None
    )
    assert (
        get_previous_revision([revision1, revision2, revision3], 22220222222222)
        == revision1
    )
    assert (
        get_previous_revision([revision1, revision2, revision3], 33330330033333)
        == revision2
    )
    assert (
        get_current_revision([revision1, revision2, revision3], 33330330033333)
        == revision3
    )
    assert get_current_revision([revision1, revision2, revision3], 0) is None


def test_get_revision_tree_wrong_filename_pattern(tmpdir: LocalPath) -> None:
    wrong_filename = Path(tmpdir) / "wrong.sql"
    wrong_filename.touch()
    with pytest.raises(ValueError):
        get_revision_tree(Path(tmpdir), MIN_VERSION, MAX_VERSION)


def test_get_revision_tree_current_equals_target() -> None:
    currentdir = Path(__file__).parent
    migrations = Path(currentdir / "revisions_tests")
    assert get_revision_tree(migrations, 20200211123456, 20200211123456) == []


@pytest.mark.parametrize(
    "driver,dsn",
    [
        ("asyncpg", TEST_DSN_POSTGRES),
        pytest.param(
            "asyncpg",
            urlparse(TEST_DSN_POSTGRES)
            ._replace(
                netloc=f"fakeuser:fakepassword@{urlparse(TEST_DSN_POSTGRES).hostname}:{urlparse(TEST_DSN_POSTGRES).port}"
            )
            .geturl(),
            marks=pytest.mark.xfail,
        ),
        pytest.param("fakedriver", "", marks=pytest.mark.xfail),
    ],
)
@pytest.mark.asyncio
async def test_get_connection(driver: str, dsn: str) -> None:
    async with get_connection(driver, dsn) as connection:
        assert connection


def test_get_revision_tree_up_to_id_included() -> None:
    currentdir = Path(__file__).parent
    migrations = Path(currentdir / "revisions_tests")
    revision1 = RevisionPath(
        version_id=11111111111111,
        message="revision1",
        file=Path(migrations / "11111111111111_revision1.sql"),
        next=22220222222222,
        previous=-1,
    )
    revision2 = RevisionPath(
        version_id=22220222222222,
        message="revision2",
        file=Path(migrations / "22220222222222_revision2.sql"),
        next=-1,
        previous=11111111111111,
    )
    rt = get_revision_tree(migrations, MIN_VERSION, 22220222222222)
    assert rt == [
        revision1,
        revision2,
    ]
