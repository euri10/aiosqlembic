# import logging
#
# import asyncpg
# import pytest
# from asyncpg.pool import Pool
#
# from aiosqlembic.schema import AsyncSchemaInspector
# from aiosqlembic.utils import create_test_database
# from tests.conftest import TEST_DSN
#
# logger = logging.getLogger(__name__)
# logging.basicConfig()
# logger.setLevel(logging.DEBUG)
#
#
# @pytest.mark.asyncio
# async def test_same_is_same_empty() -> None:
#     async with create_test_database(TEST_DSN, "d0") as d0, create_test_database(
#         TEST_DSN, "d1"
#     ) as d1:
#         try:
#             logger.debug(d0)
#             logger.debug(d1)
#             p0: Pool = await asyncpg.create_pool(dsn=d0)
#             p1: Pool = await asyncpg.create_pool(dsn=d1)
#             with open(f"tests/data/singleschema/a.sql") as f:
#                 a = f.read()
#             s0 = AsyncSchemaInspector(p0)
#             await p0.execute(a)
#             await s0.load_all()
#             s1 = AsyncSchemaInspector(p1)
#             await p1.execute(a)
#             await s1.load_all()
#             assert s0 == s1
#         finally:
#             await p1.close()
#             await p0.close()
