import logging
import re
import sys
from asyncio import AbstractEventLoop
from pathlib import Path
from typing import List, Optional, Tuple
from unittest.mock import patch
from urllib.parse import urlparse

import asyncpg
import pytest
from _pytest.logging import LogCaptureFixture
from asyncpg import Connection
from click.testing import CliRunner, Result
from py._path.local import LocalPath

from aiosqlembic.main import cli, get_current, AiosqlembicException
from aiosqlembic.models.cli import Version
from tests.conftest import TEST_DSN_POSTGRES


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def test_cli() -> None:
    runner = CliRunner()
    result = runner.invoke(cli)
    assert result.exit_code == 0
    assert """Usage: cli [OPTIONS] {asyncpg} DSN COMMAND [ARGS]...""" in result.output


def test_cli_flags_no_driver() -> None:
    runner = CliRunner()
    result = runner.invoke(
        cli,
        ["--debug"],
    )
    assert result.exit_code == 2
    assert """Error: Missing argument '{asyncpg}'. Choose from:""" in result.output


driver_data_error = [
    ("asyncpg", TEST_DSN_POSTGRES, 'syntax error at or near "schea"'),
]
driver_data_postgres_only = [("asyncpg", TEST_DSN_POSTGRES)]
driver_data_postgres_only_fake = [
    (
        "asyncpg",
        urlparse(TEST_DSN_POSTGRES)
        ._replace(
            netloc=f"fakeuser:fakepassword@{urlparse(TEST_DSN_POSTGRES).hostname}:{urlparse(TEST_DSN_POSTGRES).port}"
        )
        .geturl(),
    )
]

debugflag_data = [
    ("--debug", ["logging set to DEBUG"]),
    ("--no-debug", ["logging set to INFO"]),
]


@pytest.mark.parametrize("driver, dsn", driver_data_postgres_only)
def test_cli_flags_driver(driver: str, dsn: str) -> None:
    runner = CliRunner()
    result = runner.invoke(
        cli,
        ["--debug", driver],
    )
    logger.debug(result.output)
    assert result.exit_code == 2
    assert """Error: Missing argument 'DSN'.""" in result.output


def test_cli_flags_unknown_driver() -> None:
    runner = CliRunner()
    result = runner.invoke(
        cli,
        ["--debug", "unknown"],
    )
    logger.debug(result.output)
    assert result.exit_code == 2
    assert (
        """Error: Invalid value for '{asyncpg}': 'unknown' is not 'asyncpg'."""
        in result.output
    )


@pytest.mark.parametrize("driver,dsn", driver_data_postgres_only)
def test_cli_flags_dsn_ok(driver: str, dsn: str) -> None:
    runner = CliRunner()
    result = runner.invoke(
        cli,
        ["--debug", driver, dsn],
    )
    logger.debug(result.output)
    assert result.exit_code == 2
    assert """Error: Missing command.""" in result.output


@pytest.mark.parametrize("debugflag, log_messages", debugflag_data)
@pytest.mark.parametrize("driver,dsn", driver_data_postgres_only)
def test_create_connection_ok(
    driver: str,
    dsn: str,
    debugflag: str,
    log_messages: List[str],
    tmpdir: LocalPath,
    caplog: LogCaptureFixture,
) -> None:
    runner = CliRunner()
    migrationdir, result = create_migration(
        runner,
        tmpdir,
        driver,
        dsn,
        "testname",
        "revision1.sql",
    )
    assert [m in caplog.messages for m in log_messages]


@pytest.mark.parametrize("debugflag, log_messages", debugflag_data)
@pytest.mark.parametrize("driver,dsn", driver_data_postgres_only_fake)
def test_create_connection_not_ok(
    driver: str,
    dsn: str,
    debugflag: str,
    log_messages: List[str],
    tmpdir: LocalPath,
    caplog: LogCaptureFixture,
) -> None:
    runner = CliRunner()
    migrationdir = Path(tmpdir) / "migrations"
    assert not migrationdir.exists()
    migrationdir.mkdir()
    result = runner.invoke(
        cli,
        ["-m", migrationdir, debugflag, driver, dsn, "create", "-n", "testname"],  # type: ignore
    )
    logger.debug(result.output)
    assert result.exit_code == 1


@pytest.mark.parametrize("driver,dsn", driver_data_postgres_only)
def test_status_init(driver: str, dsn: str, tmpdir: LocalPath) -> None:
    runner = CliRunner()
    migrationdir, result = create_migration(
        runner,
        tmpdir,
        driver,
        dsn,
        "testname",
        "revision1.sql",
    )
    result = runner.invoke(cli, ["-m", migrationdir, "--debug", driver, dsn, "status"])  # type: ignore
    assert result.exit_code == 0
    logger.debug(result.output)


def create_migration(
    runner: CliRunner,
    tmpdir: LocalPath,
    driver: str,
    dsn: str,
    revision_name: str,
    sql_filename_original: str,
    version_id: Optional[int] = None,
) -> Tuple[Path, Result]:
    migrationdir = Path(tmpdir) / "migrations"
    if not migrationdir.exists():
        migrationdir.mkdir()
    if version_id:
        result = runner.invoke(
            cli,
            ["-m", migrationdir, "--debug", driver, dsn, "create", "-n", revision_name, "-v", version_id],  # type: ignore
            catch_exceptions=False,
        )
    else:
        result = runner.invoke(
            cli,
            ["-m", migrationdir, "--debug", driver, dsn, "create", "-n", revision_name],  # type: ignore
        )
    assert result.exit_code == 0
    # update file with test revision
    assert "Created: " in result.output
    m = re.match(r"(Connected to: .*\n)?Created: (.*)", result.output)
    if m is not None:
        sql_filename_created = m.group(2)
        assert Path(sql_filename_created).exists()

    with open(
        Path(__file__).parent / "revisions_tests" / driver / sql_filename_original
    ) as f:
        migrationsfile_content = f.read()
    with open(migrationdir / sql_filename_created, "w") as f:
        f.write(migrationsfile_content)
    return migrationdir, result


@pytest.mark.parametrize("driver,dsn", driver_data_postgres_only)
def test_status_after_create(driver: str, dsn: str, tmpdir: LocalPath) -> None:
    runner = CliRunner()
    migrationdir, result = create_migration(
        runner, tmpdir, driver, dsn, "revision_1", "revision1.sql"
    )
    result = runner.invoke(cli, ["-m", migrationdir, "--debug", driver, dsn, "status"])  # type: ignore
    assert result.exit_code == 0
    logger.debug(result.output)


@pytest.mark.parametrize("driver,dsn", driver_data_postgres_only)
def test_status_after_create_and_up_then_down(
    driver: str,
    dsn: str,
    tmpdir: LocalPath,
) -> None:
    runner = CliRunner()
    migrationdir, result = create_migration(
        runner, tmpdir, driver, dsn, "revision_1", "revision1.sql"
    )
    result = runner.invoke(cli, ["-m", migrationdir, "--debug", driver, dsn, "up"])  # type: ignore
    logger.debug(result.output)
    assert result.exit_code == 0

    result = runner.invoke(cli, ["-m", migrationdir, "--debug", driver, dsn, "status"])  # type: ignore
    logger.debug(result.output)
    assert result.exit_code == 0

    result = runner.invoke(cli, ["-m", migrationdir, "--debug", driver, dsn, "down"])  # type: ignore
    logger.debug(result.output)
    assert result.exit_code == 0


@pytest.mark.skipif(sys.version_info < (3, 8), reason="requires python3.8 or higher")
@pytest.mark.asyncio
async def test_get_current_no_version() -> None:
    from unittest.mock import AsyncMock

    with patch(
        "aiosqlembic.main.AiosqlembicContext", new_callable=AsyncMock
    ) as mock_settings:
        mock_settings.driver = "asyncpg"
        mock_settings.dsn = TEST_DSN_POSTGRES
        v = await get_current(mock_settings)
        assert v == Version(version_id=-1, is_applied=False)


@pytest.mark.parametrize("driver,dsn,errstr", driver_data_error)
def test_raise_error_up(
    driver: str,
    dsn: str,
    errstr: str,
    tmpdir: LocalPath,
) -> None:
    runner = CliRunner()
    migrationdir, result = create_migration(
        runner, tmpdir, driver, dsn, "revision_sql_error", "revision_up_error.sql"
    )
    result = runner.invoke(cli, ["-m", migrationdir, "--debug", driver, dsn, "up"])  # type: ignore
    assert isinstance(result.exception, AiosqlembicException)
    logger.debug(result.output)
    assert result.exit_code == 1
    assert errstr in result.output


@pytest.mark.parametrize("driver,dsn", driver_data_postgres_only)
def test_down_immediatly(
    driver: str,
    dsn: str,
    tmpdir: LocalPath,
    caplog: LogCaptureFixture,
) -> None:
    runner = CliRunner()
    migrationdir, result = create_migration(
        runner, tmpdir, driver, dsn, "revision_1", "revision1.sql"
    )
    result = runner.invoke(cli, ["-m", migrationdir, "--debug", driver, dsn, "down"])  # type: ignore
    assert isinstance(result.exception, AiosqlembicException)
    logger.debug(result.output)
    assert result.exit_code == 1
    assert "Error: You're at min rev on file: None" in result.output


async def check_in_db(version_id: int, idx: int) -> None:
    conn: Connection = await asyncpg.connect(TEST_DSN_POSTGRES)
    q = """select * from aiosqlembic.aiosqlembic"""
    r = await conn.fetch(q)
    assert r[idx]["version_id"] == version_id
    assert r[idx]["is_applied"] is True
    await conn.close()


@pytest.mark.parametrize("driver,dsn", driver_data_postgres_only)
def test_status_after_successive_up1(
    driver: str, dsn: str, tmpdir: LocalPath, event_loop: AbstractEventLoop
) -> None:
    runner = CliRunner()
    version_id = 30000101000000
    migrationdir, result = create_migration(
        runner, tmpdir, driver, dsn, "revision_1", "revision2.sql", version_id
    )
    result = runner.invoke(cli, ["-m", migrationdir, "--debug", driver, dsn, "up1"])  # type: ignore
    logger.debug(result.output)
    assert result.exit_code == 0

    result = runner.invoke(cli, ["-m", migrationdir, "--debug", driver, dsn, "status"])  # type: ignore
    logger.debug(result.output)
    assert result.exit_code == 0
    logger.debug(result.output)
    assert str(version_id) in result.output

    event_loop.run_until_complete(check_in_db(version_id, 1))

    result = runner.invoke(cli, ["-m", migrationdir, "--debug", driver, dsn, "down"])  # type: ignore
    assert result.exit_code == 0


@pytest.mark.parametrize("driver,dsn", driver_data_postgres_only)
def test_status_after_upTo_then_up(
    driver: str, dsn: str, tmpdir: LocalPath, event_loop: AbstractEventLoop
) -> None:
    runner = CliRunner()
    version_ids = [("revision1", 11111111111111, 1), ("revision2", 22220222222222, 2)]
    for vid_name, vid, vid_test_idx in version_ids:
        migrationdir, result = create_migration(
            runner, tmpdir, driver, dsn, vid_name, f"revision{vid_test_idx}.sql", vid
        )

    result = runner.invoke(cli, ["-m", migrationdir, "--debug", driver, dsn, "up", "-v", 11111111111111])  # type: ignore
    logger.debug(result.output)
    assert result.exit_code == 0

    event_loop.run_until_complete(check_in_db(11111111111111, 1))

    result = runner.invoke(cli, ["-m", migrationdir, "--debug", driver, dsn, "up"])  # type: ignore
    logger.debug(result.output)
    assert result.exit_code == 0

    event_loop.run_until_complete(check_in_db(22220222222222, 2))

    result = runner.invoke(cli, ["-m", migrationdir, "--debug", driver, dsn, "down"])  # type: ignore
    assert result.exit_code == 0
    result = runner.invoke(cli, ["-m", migrationdir, "--debug", driver, dsn, "down"])  # type: ignore
    assert result.exit_code == 0


@pytest.mark.parametrize("driver,dsn,errstr", driver_data_error)
def test_raise_error_down(
    driver: str,
    dsn: str,
    errstr: str,
    tmpdir: LocalPath,
) -> None:
    runner = CliRunner()
    migrationdir, result = create_migration(
        runner, tmpdir, driver, dsn, "revision_sql_error", "revision_down_error.sql"
    )

    result = runner.invoke(cli, ["-m", migrationdir, "--debug", driver, dsn, "up"])  # type: ignore
    logger.debug(result.output)
    assert result.exit_code == 0

    result = runner.invoke(cli, ["-m", migrationdir, "--debug", driver, dsn, "down"])  # type: ignore
    assert isinstance(result.exception, AiosqlembicException)
    logger.debug(result.output)
    assert result.exit_code == 1
    assert errstr in result.output
