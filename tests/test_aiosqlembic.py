import platform

from click.testing import CliRunner

from aiosqlembic import __version__
from aiosqlembic.main import cli


def test_version_command() -> None:
    runner = CliRunner()
    result = runner.invoke(cli, ["--version"])
    assert result.exit_code == 0
    assert result.output == "Running aiosqlembic %s with %s %s on %s\n" % (
        __version__,
        platform.python_implementation(),
        platform.python_version(),
        platform.system(),
    )
