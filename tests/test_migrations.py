# import logging
# from typing import Tuple
#
# import asyncpg
# import pytest
# from asyncpg.pool import Pool
#
# from aiosqlembic.schema import Amigration
# from tests.conftest import TEST_DSN
# from aiosqlembic.utils import create_test_database
#
# logger = logging.getLogger(__name__)
# logging.basicConfig()
# logger.setLevel(logging.DEBUG)
#
#
# def rf(dir: str) -> Tuple[str, str, str]:
#     with open(f"tests/data/{dir}/a.sql") as f:
#         a = f.read()
#     with open(f"tests/data/{dir}/b.sql") as f:
#         b = f.read()
#     with open(f"tests/data/{dir}/expected.sql") as f:
#         expected = f.read()
#     return a, b, expected
#
#
# sqldata = [(rf("singleschema"))]
#
#
# @pytest.mark.parametrize("a,b,expected", sqldata)
# @pytest.mark.asyncio
# async def test_diff_schemas(a, b, expected) -> None:  # type: ignore
#     async with create_test_database(TEST_DSN, "d0") as d0, create_test_database(
#         TEST_DSN, "d1"
#     ) as d1:
#         try:
#             logger.debug(d0)
#             logger.debug(d1)
#             p0: Pool = await asyncpg.create_pool(dsn=d0)
#             result0 = await p0.execute(a)
#             p1: Pool = await asyncpg.create_pool(dsn=d1)
#             result1 = await p1.execute(b)
#             logger.info("RESULTS")
#             logger.debug(result0)
#             logger.debug(result1)
#
#             m = await Amigration().create(p0, p1)
#             m.changes.i_from.one_schema("goodschema")
#             m.changes.i_target.one_schema("goodschema")
#             m.set_safety(False)
#             m.add_extension_changes(creates=False, drops=False)
#             m.add_all_changes(privileges=False)
#             logger.debug(m.statements)
#             ss = m.sql
#             logger.debug(ss)
#             assert ss.strip() == expected
#         except Exception as e:
#             logger.error(e)
#         finally:
#             await p1.close()
#             await p0.close()
#             logger.info("END")
