import logging
import os
from typing import AsyncGenerator

import pytest

from aiosqlembic.utils import create_test_database

logger = logging.getLogger(__name__)

TEST_DSN: str = os.environ["TEST_DSN"]
TEST_DSN_POSTGRES: str = os.environ["TEST_DSN_POSTGRES"]

logger.info(f"TEST_DSN: {TEST_DSN}")


@pytest.fixture(scope="function", autouse=True)
async def get_test_database_postgresql() -> AsyncGenerator[str, None]:
    async with create_test_database(TEST_DSN, "postgres_test") as db:
        yield db
