-- name: up_2#
create sequence "public"."table_name_column_1_seq";

alter table "public"."table_name" alter column "column_1" set default nextval('table_name_column_1_seq'::regclass);

alter table "public"."table_name" alter column "column_1" set not null;

CREATE UNIQUE INDEX table_name_pk ON public.table_name USING btree (column_1);

alter table "public"."table_name" add constraint "table_name_pk" PRIMARY KEY using index "table_name_pk";



-- name: down_1#
alter table "public"."table_name" drop constraint "table_name_pk";

drop index if exists "public"."table_name_pk";

alter table "public"."table_name" alter column "column_1" drop default;

alter table "public"."table_name" alter column "column_1" drop not null;

drop sequence if exists "public"."table_name_column_1_seq";

