Installation
============

In a virtual environnement just run

.. code-block:: python

    pip install aiosqlembic

or use your favorite package manager, pipenv, poetry, pip-tools, no preferred order, you choose it.

Contribute
==========

Prefered way to contribute would be to clone the repo and install everything with:

.. code-block:: python

    poetry install

A pre-commit file is available, it will enforce same rules used in tests CI, you may choose to use it :)
