<img src="/docs/source/Cigogne_Belon_1555.jpg"  height="200">

**Aiosqlembic, migrations welcome !**


Readme
======

![pipeline](https://gitlab.com/euri10/aiosqlembic/badges/master/pipeline.svg)
![coverage](https://gitlab.com/euri10/aiosqlembic/badges/master/coverage.svg)
![documentation](https://readthedocs.org/projects/aiosqlembic/badge/?version=latest)


Aiosqlembic aims at running database migrations powered by the awesome [aiosql](https://github.com/nackjicholson/aiosql)

It's inspired by goose for those coming from go

It is in development and likely to break

Documentation
-------------

It's [here](https://aiosqlembic.readthedocs.io/en/latest/index.html)

Usage
-----

Run `aiosqlembic --help`
